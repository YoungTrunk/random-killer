'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Press = function () {
  function Press() {
    _classCallCheck(this, Press);
  }

  _createClass(Press, [{
    key: 'handleEvent',
    value: function handleEvent(event) {
      var waitingForClick = false;
      switch (event.type) {
        case 'click':
          waitingForClick = setTimeout(function () {
            playGame();
          }, 500);
          break;
      }
    }
  }]);

  return Press;
}();

var press = new Press();
var playButton = document.getElementById('play-button-a');
playButton.addEventListener('click', press);

function playGame() {
  var players = document.getElementById("count").value;

  var max = Number(players);
  if (max === 0) {
    alert("Введите количество игроков!");
    return;
  }

  var resultDiv = document.getElementById("result");
  resultDiv.value = "Происходит вычисление неудачника...";

  var splash = setInterval(function () {
    resultDiv.style.opacity = 1 - (resultDiv.style.opacity || 1);
  }, 600);

  setTimeout(function () {
    clearInterval(splash);

    var randNumber = Math.round(getRandomArbitrary(1, max));
    resultDiv.style.fontWeight = "bold";
    resultDiv.value = '\u0418\u0433\u0440\u043E\u043A \u043F\u043E\u0434 \u043D\u043E\u043C\u0435\u0440\u043E\u043C ' + randNumber + ' \u043F\u0440\u043E\u0438\u0433\u0440\u0430\u043B!';
  }, 4000);
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}